import 'dart:io';

import 'package:dart_mid/dart_mid.dart' as dart_mid;

List<String> infix = [];
List<String> postfix = [];
List<String> operator = [];
List<int> answer = [];

void main(List<String> arguments) {
  print("Enter an expression in the Infix form: : ");
  var input = stdin.readLineSync()!; //infix
  //print(listInput(input));

  listInput(input);
  print("infix : ");
  print(infix);

  infixToPostfix(infix);

  print("Postfix : ");
  print(postfix);

  result(postfix);
  print("Result : ");
  print(answer);
}

void result(List<String> opera) {
  for (var i = 0; i < postfix.length; i++) {
    if (opera[i] == "+" ||
        opera[i] == "-" ||
        opera[i] == "*" ||
        opera[i] == "/" ||
        opera[i] == "^") {
      int lf = answer.removeLast();
      int rt = answer.removeLast();
      int sum = 1;
      switch (opera[i]) {
        case "+":
          answer.add(lf + rt);
          break;
        case "*":
          answer.add(lf * rt);
          break;
        case "/":
          answer.add(lf ~/ rt);
          break;
        case "^":
          for (int i = 0; i < rt; i++) {
            sum *= lf;
          }
          answer.add(sum);
          break;
      }
    } else {
      answer.add(int.parse(opera[i]));
    }
  }
}

void infixToPostfix(List<String> infix) {
  for (var i = 0; i < infix.length; i++) {
    if (infix[i] == "+" ||
        infix[i] == "-" ||
        infix[i] == "*" ||
        infix[i] == "/" ||
        infix[i] == "^") {
      while (!operator.isEmpty &&
          !(operator.last == "(") &&
          (infix[i] == "+" ||
              infix[i] == "-" && operator.last == "*" ||
              operator.last == "/" ||
              operator.last == "^")) {
        postfix.add(operator.last);
        operator.removeLast();
      }

      while (!operator.isEmpty &&
          !(operator.last == "(") &&
          (infix[i] == "*" ||
              infix[i] == "/" && operator.last == "*" ||
              operator.last == "/" ||
              operator.last == "^")) {
        postfix.add(operator.last);
        operator.removeLast();
      }

      operator.add(infix[i]);
    } else if (infix[i] == "(") {
      operator.add(infix[i]);
    } else if (infix[i] == ")") {
      while (!(operator.last == "(")) {
        postfix.add(operator.last);
        operator.removeLast();
      }
      operator.removeLast();
    } else {
      postfix.add(infix[i]);
    }
  }
  while (!operator.isEmpty) {
    postfix.add(operator.last);
    operator.removeLast();
  }
}

List<String> listInput(var input) {
  infix = input.split(' ');
  //infix.add(x);
  return infix;
}

//